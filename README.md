# recipe-app-api-proxy

NGINX proxy for our Django API. New test for username

## Usage

### Environment Variables

* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - hostname of the port that the proxy forwards requests to (default: `app`)
* `APPP_PORT` - port of the app to forward the requests to from the proxy (default: `9000`)
